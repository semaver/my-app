import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
//import  "./Form";

class App extends Component {
  handle(event) {
    console.log(event.target.value)
  }
  render() {
    return (
      <div className="App">
        <input type ="text" onChange ={ this.handle.bind(this)}/> 
      </div>
    );
  }
}


export default App;
