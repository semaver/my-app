import './style.css'
import React, { Component } from 'react';

class Converter extends Component {
    constructor() {
        super();
        this.state = {
            velocity: '',
            result: ''
        }
    }
    convertWind = (event) => {
        //console.log(this.refs.velocity.value);
        this.setState({
            result:Math.ceil(Math.pow( this.state.velocity / 0.8360,2/3))
        });
    }
    
    render() {
       
        return(
        <React.Fragment>
                <h1>Convert wind</h1>
                <div id="pane">
                
                <form class ="box">
                    Velocity : <input id = 'velocity'
                        placeholder=" velocity" 
                        value={this.state.velocity}
                        onChange={e => this.setState({velocity: e.target.value})} />
                </form>
                <form class = "box">
                        Beaufort : <input
                        placeholder="  get result" id='beaufort'
                        value={this.state.result }
                        onChange={e => this.setState({result: e.target.value})} />
                    </form>
                    <button onClick={this.convertWind}>convert</button>
                </div>
            </React.Fragment >
            
        );
      
    }
   
   
    
}
 
export default Converter;